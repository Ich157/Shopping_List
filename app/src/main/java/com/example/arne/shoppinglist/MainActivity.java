package com.example.arne.shoppinglist;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends Activity implements ShopItemDialogFragment.TextEntryDialogListener{

    ListView shoppingList;
    SQLiteDatabase db;
    Cursor cursor;
    double total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find List View
        shoppingList = (ListView) findViewById(R.id.shoppingList);
        registerForContextMenu(shoppingList);

        db = (new DatabaseOpenHelper(this)).getWritableDatabase();

    }

    public void addItem(View view)
    {
        ShopItemDialogFragment eDialog = new ShopItemDialogFragment();
        eDialog.show(getFragmentManager(), "Add Product");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String product, int count, double price) {
        ContentValues values=new ContentValues(3);
        values.put("name", product);
        values.put("count", count);
        values.put("price",price);
        db.insert("ShoppingList", null, values);
        // get data again
        queryData();
    }

    public void queryData() {

        // get data with query
        String[] resultColumns = new String[]{"_id","name","count","price"};
        cursor = db.query("ShoppingList",resultColumns,null,null,null,null,"_id DESC",null);

        // add data to adapter
        ListAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.list_item, cursor,
                new String[] {"name", "count","price"},      // from
                new int[] {R.id.name, R.id.count, R.id.price}    // to
                ,0);  // flags

        // show data in listView
        shoppingList.setAdapter(adapter);

        cursor = db.rawQuery("SELECT sum(price*count)as Total FROM ShoppingList",null);

        while(cursor.moveToNext())
        {
            total = cursor.getDouble(0);
        }

        Toast.makeText(this,"Total: "+ total,Toast.LENGTH_LONG).show();


    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        queryData();
    }
}
